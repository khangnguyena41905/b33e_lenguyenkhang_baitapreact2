import "./App.css";

import RenderList from "./Component/RenderList";

function App() {
  return (
    <div className="App">
      <RenderList />
    </div>
  );
}

export default App;
