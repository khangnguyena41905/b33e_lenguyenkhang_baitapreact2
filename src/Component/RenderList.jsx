import React, { Component } from "react";
import { data } from "./dataGlasses";
import style from "./component.module.css";
export default class RenderList extends Component {
  state = {
    glasses: data,
    currentGlasses: data[0],
    fisrtChoose: true,
  };
  renderGlass = () => {
    if (this.state.fisrtChoose) {
      return <div></div>;
    }
    return (
      <div>
        <div className={style.glass}>
          <img src={this.state.currentGlasses.url} alt="" />
        </div>
        <div className={style.content_info}>
          <h1>{this.state.currentGlasses.name}</h1>
          <h2>{this.state.currentGlasses.price}</h2>
          <p>{this.state.currentGlasses.desc}</p>
        </div>
      </div>
    );
  };
  handleChangeGlasses = (item) => {
    this.setState({
      currentGlasses: item,
      fisrtChoose: false,
    });
  };
  renderList = () => {
    return this.state.glasses.map((item) => {
      return (
        <img
          onClick={() => {
            this.handleChangeGlasses(item);
          }}
          style={{
            width: "20%",
            display: "inline-block",
            marginTop: "20px",
          }}
          src={item.url}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div>
        <div>
          <div className={style.modelComponent}>
            <img src="../glassesImage/model.jpg" alt="" />
            {this.renderGlass()}
          </div>
        </div>
        <div className="w-3/4 p-5 bg-gray-400 mx-auto">{this.renderList()}</div>
      </div>
    );
  }
}
